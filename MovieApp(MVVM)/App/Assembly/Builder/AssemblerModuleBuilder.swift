//
//  AssemblerModuleBuilder.swift
//  MovieApp(MVVM)
//
//  Created by Denys Nikolaichuk on 14.05.2021.
//

import Foundation
import UIKit

///
protocol AssemblerBuilderProtocol {
    func createMainModule(coordinator: CoordinatorProtocol) -> UIViewController
    func createDetailModule(movie: ResultMovie?, coordinator: CoordinatorProtocol) -> UIViewController
}

final class AssemblerModuleBuilder: AssemblerBuilderProtocol {
    func createMainModule(coordinator: CoordinatorProtocol) -> UIViewController {
        let networkService = NetworkService()
        let photoServise = PhotoService()
        let vc = MainViewController.instantiate()
        vc.viewModel = MoviesViewModel(
            networkService: networkService,
            coordinator: coordinator,
            photoService: photoServise
        )
        vc.viewModel?.coordinator = coordinator
        vc.viewModel?.delegate = vc
        return vc
    }

    func createDetailModule(movie: ResultMovie?, coordinator: CoordinatorProtocol) -> UIViewController {
        let networkService = NetworkService()
        let photoServise = PhotoService()
        let vc = DetailViewController.instantiate()
        vc.viewModel = DetailViewModel(movieDetails: movie, networkService: networkService, photoService: photoServise)
        vc.viewModel?.coordinator = coordinator
        return vc
    }
}
