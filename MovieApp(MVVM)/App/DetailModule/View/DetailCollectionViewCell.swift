//
//  DetailCollectionViewCell.swift
//  MovieApp(MVP)
//
//  Created by Denys Nikolaichuk on 05.05.2021.
//

import UIKit

///
final class DetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet var movieImage: UIImageView!

    func setMovie(_ movie: UIImage) {
        movieImage.image = movie
    }
}
