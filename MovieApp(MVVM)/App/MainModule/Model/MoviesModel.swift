//
//  MoviesModel.swift
//  MovieApp(MVVM)
//
//  Created by Denys Nikolaichuk on 11.05.2021.
//

import Foundation

///
struct MoviesModel {
    var results: [ResultMovie]
}
